package mainPackage;

import static spark.Spark.*;

/**
 * HelloWorld description
 *
 * @author johnson
 * @since 2015-06-17
 */
public class HelloWorld {

    public static void main(String[] args) {
        get("/hello", (req, res) -> "Hello Spark!!");
    }
}
